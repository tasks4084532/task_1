package org.example;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.example.exceptions.InvalidIndexException;

@Getter
@Setter
public class MyList extends DataNode{
    private Node head;
    private static int inLevel = 0;

    public MyList(DataNode newData){
        head = new Node(newData, null);
    }

    @Override
    public void print(){
        Node current = head;
        while (current.next != null){
            if (current.data instanceof MyList) {
                inLevel++;
            } else{
                System.out.print("--".repeat(inLevel));
            }

            current.data.print();
            current = current.next;
        }

        System.out.print("--".repeat(inLevel));
        current.data.print();
        if (inLevel != 0){inLevel--;}
    }
    @JsonIgnore
    public void getJson(String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        var data = objectMapper.writeValueAsString(this);
        FileWriter myWriter = new FileWriter(path);
        myWriter.write(data);
        myWriter.close();
    }
    public void add(DataNode taskData){head = new Node(taskData, head);}

    public void add(DataNode taskData, int index) throws InvalidIndexException {
        if (index < 0){
            throw new InvalidIndexException("Index can't be negative");
        }
        else if (index == 0){head = new Node(taskData, head);}
        else {
            Node current = head;
            int i = 0;
            while (i != index-1) {
                if (current.next == null){
                    throw new InvalidIndexException("List is too small for adding element with that index");
                }
                i++;
                System.out.println("i = " + i);
                current = current.next;
            }
            if (current.next == null){
                current.next = new Node(taskData, null);
            } else {
                current.next = new Node(taskData, current.next);
            }
        }
    }

    public void remove(DataNode taskData){
        if (head.data == taskData){
            head = head.next;
        } else {
            Node current = head;
            while (current.next != null){
                if (current.next.data == taskData){
                    break;
                }
                current = current.next;
            }
            if (current.next != null) {
                current.next = current.next.next;
            }
        }
    }

    public int length(){
        Node current = head;
        int index = 1;
        while (current.next != null){
            index++;
            current = current.next;
        }
        return index;
    }

    public int find(DataNode dataToSearch){
        Node current = head;
        int index = 0;
        boolean exist = false;
        while (true) {
            if (current.data == dataToSearch) {
                exist = true;
                break;
            }
            if (current.next == null){break;}

            current = current.next;
            index++;
        }
        if (exist){
            return index;
        } else {
            return -1;
        }
    }
}
