package org.example.exceptions;

public class InvalidIndexException extends Exception{
    public InvalidIndexException(String message) {
        super("List index is out of range");
    }
}
