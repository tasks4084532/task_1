package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Bird extends DataNode{
    private boolean canFly;
    private String voice;
    @Override
    public void print() {
        System.out.println("name: Bird; tail: " + canFly + "; voice: " + voice);
    }
}
