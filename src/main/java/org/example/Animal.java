package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Animal extends DataNode{
    private boolean tail;
    private int pawNumber;
    @Override
    public void print() {
        System.out.println("name: Animal; tail: " + tail + "; pawNumbers: " + pawNumber);
    }
}
