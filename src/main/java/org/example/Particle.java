package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Particle extends DataNode{
    private String charge;
    private boolean isLepton;
    @Override
    public void print() {
        System.out.println("name: Particle; charge: " + charge + "; isLepton: " + isLepton);
    }
}
