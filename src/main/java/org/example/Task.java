package org.example;

import org.example.exceptions.InvalidIndexException;

import java.io.IOException;

class Task {
    public static void main(String[] args) throws InvalidIndexException, IOException {
        Animal dog = new Animal(true, 4);
        Bird cock = new Bird(false, "cuckoo");
        Animal man = new Animal(false, 4);


        MyList firstList = new MyList(dog);
        firstList.add(man);
        MyList zeroList = new MyList(dog);
        zeroList.add(dog);
        firstList.add(zeroList);
        firstList.add(cock);


        MyList secondList = new MyList(dog);
        secondList.add(firstList);
        secondList.add(new Animal(false,0));
        secondList.print();

    }
}
