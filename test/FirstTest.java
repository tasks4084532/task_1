import org.example.Animal;
import org.example.Bird;
import org.example.MyList;
import static org.junit.Assert.assertEquals;

import org.example.Particle;
import org.example.exceptions.InvalidIndexException;
import org.junit.Test;

public class FirstTest {
    @Test
    public void Test() throws InvalidIndexException {

        Animal dog = new Animal(true, 4);
        Bird cock = new Bird(false, "cuckoo");
        Animal man = new Animal(false, 4);
        Animal babayka = new Animal(false, 3);

        MyList firstList = new MyList(dog);
        firstList.add(man);
        firstList.add(cock);

        assertEquals(1, firstList.find(man));
        assertEquals(0, firstList.find(cock));
        assertEquals(2, firstList.find(dog));
        assertEquals(3, firstList.length());

        firstList.add(babayka, 1);

        assertEquals(2, firstList.find(man));
        assertEquals(0, firstList.find(cock));
        assertEquals(3, firstList.find(dog));
        assertEquals(1, firstList.find(babayka));
        assertEquals(4, firstList.length());

        Particle electron = new Particle("-", true);
        Bird sparrow = new Bird(true, "tweet-tweet");
        MyList secondList = new MyList(sparrow);
        secondList.add(cock);

        MyList thirdList = new MyList(secondList);

        MyList forthList = new MyList(thirdList);
        forthList.add(electron);
    }
}
